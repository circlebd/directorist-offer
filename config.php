<?php
// Plugin version.
if (!defined('ATOA_VERSION')) {
    define('ATOA_VERSION', '1.0.0');
}
// Plugin Folder Path.
if (!defined('ATOA_DIR')) {
    define('ATOA_DIR', plugin_dir_path(__FILE__));
}
// Plugin Folder URL.
if (!defined('ATOA_URL')) {
    define('ATOA_URL', plugin_dir_url(__FILE__));
}
// Plugin Root File.
if (!defined('ATOA_FILE')) {
    define('ATOA_FILE', __FILE__);
}
if (!defined('ATOA_BASE')) {
    define('ATOA_BASE', plugin_basename(__FILE__));
}
// Plugin Includes Path
if (!defined('ATOA_INC_DIR')) {
    define('ATOA_INC_DIR', ATOA_DIR . 'inc/');
}
// Plugin Classes Path
if (!defined('ATOA_CLASSES_DIR')) {
    define('ATOA_CLASSES_DIR', ATOA_DIR . 'inc/classes/');
}
// Plugin Assets Path
if (!defined('ATOA_ASSETS')) {
    define('ATOA_ASSETS', ATOA_URL . 'assets/');
}
// Plugin Template Path 
if (!defined('ATOA_TEMPLATES_DIR')) {
    define('ATOA_TEMPLATES_DIR', ATOA_DIR . 'templates/');
}
// Plugin Language File Path
if (!defined('ATOA_LANG_DIR')) {
    define('ATOA_LANG_DIR', dirname(plugin_basename(__FILE__)) . '/languages');
}
// Plugin Name
if (!defined('ATOA_NAME')) {
    define('ATOA_NAME', 'Directorist - Pricing Plans');
}
