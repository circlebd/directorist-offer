<?php

// atoa_get_template
function atoa_get_template($template_file, $args = array())
{
    if (is_array($args)) {
        extract($args);
    }

    $theme_template  = '/directorist-offer-addon/' . $template_file . '.php';
    $plugin_template = ATOA_TEMPLATES_DIR . $template_file . '.php';

    if (file_exists(get_stylesheet_directory() . $theme_template)) {
        $file = get_stylesheet_directory() . $theme_template;
    } elseif (file_exists(get_template_directory() . $theme_template)) {
        $file = get_template_directory() . $theme_template;
    } else {
        $file = $plugin_template;
    }


    if (file_exists($file)) {
        include $file;
    }
}

function atoa_get_template_part($template, $data = array())
{

    $template = '/templates/' . $template . '.php';

    $file = ATOA_DIR . $template;

    require $file;
}
