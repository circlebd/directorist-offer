<?php
defined('ABSPATH') || die('Direct access is not allowed.');
/**
 * @since 1.7.4
 * @package Directorist
 */

if (!class_exists('ATOA_Enqueuer')) :
    class ATOA_Enqueuer
    {
        public function __construct()
        {
            add_action('admin_enqueue_scripts', array($this, 'register_required_admin_scripts'));
            add_action('wp_enqueue_scripts', array($this, 'register_required_scripts'));
        }

        public function register_required_admin_scripts($admin_page)
        {
            global $post;
            if ((('post.php' == $admin_page) || ('post-new.php' == $admin_page))  && 'atbdp_offers' == $post->post_type) {
                /* wp_enqueue_script('atpp-admin-validator-script', ATPP_ASSETS . '/js/plan-validitor-admin.js', array('jquery'), true);
                $data = array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                );                
                wp_localize_script( 'atpp-admin-validator-script', 'validator_admin_js', $data ); */
                //wp_enqueue_style('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css');
                //wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array('jquery'));

                wp_enqueue_script('admin_post_type', ATOA_ASSETS . '/js/admin-post-type.js', array('jquery'));
            }
        }

        public function register_required_scripts()
        {
            wp_enqueue_style('directorist-offer-main', ATOA_ASSETS . '/css/main.css');
        }
    }
    
endif;

new ATOA_Enqueuer;