<?php
defined('ABSPATH') || die('Direct access is not allowed.');
/**
 * @since 1.7.4
 * @package Directorist
 */
if (!class_exists('ATOA_Data_Controller')) :
    class ATOA_Data_Controller
    {
        public function __construct()
        {
            add_action('add_meta_boxes', array($this, 'atoa_add_meta_boxes'));
            add_action('save_post', array($this, 'atoa_save_meta_data'));
        }

        /**
         * Register Meta Boxes
         * 
         * @since 1.0.0
         * @access public
         */

        public function atoa_add_meta_boxes()
        {
            add_meta_box(
                'atbdp-offer-details',
                __('Basic Information', 'directorist-offer-addon'),
                array($this, 'atbdp_meta_box_offer_details'),
                'atbdp_offers',
                'normal',
                'high'
            );
        }

        /**
         * Basic Information Meta Box
         * 
         * @since 1.0.0
         * @access public
         */

        public function atbdp_meta_box_offer_details($post)
        {
            // Add a nonce field so we can check for it later
            wp_nonce_field('atbdp_save_offer_details', 'atbdp_offer_details_nonce');
            /**
             * Display the "Field Details" meta box.
             */

            ATBDP_Offer_Addon()->load_template('admin-meta-fields', ['post' => $post]);
        }

        public function atoa_save_meta_data($post_id)
        {
            if (!isset($_POST['post_type'])) {
                return $post_id;
            }


            // If this is an autosave, our form has not been submitted, so we don't want to do anything
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return $post_id;
            }

            // Check the logged in user has permission to edit this post
            if (!current_user_can('edit_post', $post_id)) {
                return $post_id;
            }

            // Check if "atbdp_offer_details_nonce" nonce is set
            if (isset($_POST['atbdp_offer_details_nonce'])) {

                // Verify that the nonce is valid
                if (wp_verify_nonce($_POST['atbdp_offer_details_nonce'], 'atbdp_save_offer_details')) {

                    $offer_type =  !empty($_POST['offer_type']) ? $_POST['offer_type'] : '';
                    update_post_meta($post_id, 'offer_type', $offer_type);

                    $offer_listing =  !empty($_POST['offer_listing']) ? $_POST['offer_listing'] : '';
                    update_post_meta($post_id, 'offer_listing', $offer_listing);

                    $offer_coupon_code =  !empty($_POST['offer_coupon_code']) ? $_POST['offer_coupon_code'] : '';
                    update_post_meta($post_id, 'offer_coupon_code', $offer_coupon_code);

                    $offer_discount_type = isset($_POST['offer_discount_type']) ? $_POST['offer_discount_type'] : '';
                    update_post_meta($post_id, 'offer_discount_type', $offer_discount_type);

                    $offer_discount_amount = isset($_POST['offer_discount_amount']) ? $_POST['offer_discount_amount'] : '0';
                    update_post_meta($post_id, 'offer_discount_amount', $offer_discount_amount);

                    $offer_get_item = isset($_POST['offer_get_item']) ? $_POST['offer_get_item'] : '0';
                    update_post_meta($post_id, 'offer_get_item', $offer_get_item);

                    $offer_get_qty = isset($_POST['offer_get_qty']) ? $_POST['offer_get_qty'] : '0';
                    update_post_meta($post_id, 'offer_get_qty', $offer_get_qty);

                    $offer_buy_item = isset($_POST['offer_buy_item']) ? $_POST['offer_buy_item'] : '0';
                    update_post_meta($post_id, 'offer_buy_item', $offer_buy_item);

                    $offer_buy_qty = isset($_POST['offer_buy_qty']) ? $_POST['offer_buy_qty'] : '0';
                    update_post_meta($post_id, 'offer_buy_qty', $offer_buy_qty);

                    $offer_discount_condition = isset($_POST['offer_discount_condition']) ? sanitize_textarea_field($_POST['offer_discount_condition']) : '';
                    update_post_meta($post_id, 'offer_discount_condition', $offer_discount_condition);

                    $offer_short_description = isset($_POST['offer_short_description']) ? sanitize_textarea_field($_POST['offer_short_description']) : '';
                    update_post_meta($post_id, 'offer_short_description', $offer_short_description);

                    $offer_length = isset($_POST['offer_length']) ? (int)$_POST['offer_length'] : '0';
                    update_post_meta($post_id, 'offer_length', $offer_length);

                    $offer_length_unl = isset($_POST['offer_length_unl']) ? sanitize_text_field($_POST['offer_length_unl']) : '';
                    update_post_meta($post_id, 'offer_length_unl', $offer_length_unl);

                    $offer_period_term = isset($_POST['offer_period_term']) ? sanitize_text_field($_POST['offer_period_term']) : '';
                    update_post_meta($post_id, '_offer_period_term', $offer_period_term);

                    $offer_start_date = isset($_POST['offer_start_date']) ? sanitize_text_field($_POST['offer_start_date']) : '';
                    update_post_meta($post_id, 'offer_start_date', $offer_start_date);

                    $offer_end_date = isset($_POST['offer_end_date']) ? sanitize_text_field($_POST['offer_end_date']) : '';
                    update_post_meta($post_id, 'offer_end_date', $offer_end_date);
                }
            }

            return $post_id;
        }
    }
endif;
