<?php
defined('ABSPATH') || die('Direct access is not allowed.');

/**
 * @since 1.0.0
 * @package Directorist
 */

if (!class_exists('ATOA_CPTs')) :
    class ATOA_CPTs
    {
        public function __construct()
        {
            add_action('init', array($this, 'register_custom_post_type_offers'));
            add_action('wp_ajax_getlisting', array($this, 'rudr_get_posts_ajax_callback')); // wp_ajax_{action}
        }

        /**
         * Register Custom Post Type Deals
         * 
         * @since 1.0.0
         * @access public
         */
        public function register_custom_post_type_offers()
        {
            $labels = array(
                'name' => _x('Offers', 'Post Type General Name', 'directorist-offer-addon'),
                'singular_name' => _x('Offer', 'Post Type Singular Name', 'directorist-offer-addon'),
                'menu_name' => __('Offers', 'directorist-offer-addon'),
                'name_admin_bar' => __('Offer', 'directorist-offer-addon'),
                'all_items' => __('Offers', 'directorist-offer-addon'),
                'add_new_item' => __('Add New Offer', 'directorist-offer-addon'),
                'add_new' => __('Add New Offer', 'directorist-offer-addon'),
                'new_item' => __('New Offer', 'directorist-offer-addon'),
                'edit_item' => __('Edit Offer', 'directorist-offer-addon'),
                'update_item' => __('Update Offer', 'directorist-offer-addon'),
                'view_item' => __('View Offer', 'directorist-offer-addon'),
                'search_items' => __('Search Offer', 'directorist-offer-addon'),
                'not_found' => __('No DeaOfferl found', 'directorist-offer-addon'),
                'not_found_in_trash' => __('No Offer found in Trash', 'directorist-offer-addon'),
            );

            $args = array(
                'labels' => $labels,
                'description' => __('This post type will be created as deals and offers under the listings', 'directorist-offer-addon'),
                'supports' => array('title', 'thumbnail'),
                'taxonomies' => array(''),
                'hierarchical' => false,
                'public' => true,
                'show_ui' => current_user_can('manage_atbdp_options') ? true : false, // show the menu only to the admin
                'show_in_menu' => current_user_can('manage_atbdp_options') ? 'edit.php?post_type=at_biz_dir' : false,
                'show_in_admin_bar' => true,
                'show_in_nav_menus' => true,
                'can_export' => true,
                'has_archive' => true,
                'exclude_from_search' => true,
                'publicly_queryable' => true,
                'capability_type' => 'at_biz_dir',
                'map_meta_cap' => true,
            );

            register_post_type('atbdp_offers', $args); //ATBDP_Offer_Addon
        }



        public function rudr_get_posts_ajax_callback()
        {

            // we will pass post IDs and titles to this array
            $return = array();

            // you can use WP_Query, query_posts() or get_posts() here - it doesn't matter
            $search_results = new WP_Query(array(
                's' => $_GET['q'], // the search query
                'post_status' => 'publish', // if you don't want drafts to be returned
                'ignore_sticky_posts' => 1,
                'posts_per_page' => 50 // how much to show at once
            ));
            if ($search_results->have_posts()) :
                while ($search_results->have_posts()) : $search_results->the_post();
                    // shorten the title a little
                    $title = (mb_strlen($search_results->post->post_title) > 50) ? mb_substr($search_results->post->post_title, 0, 49) . '...' : $search_results->post->post_title;
                    $return[] = array($search_results->post->ID, $title); // array( Post ID, Post Title )
                endwhile;
            endif;
            echo json_encode($return);
            die;
        }
    }

endif;
