<?php
defined('ABSPATH') || die('Direct access is not allowed.');
/**
 * @author  mahfuz
 * @since   1.0
 * @version 1.0
 */

class Custom_Offer_Fields
{

    public function __construct()
    {
        //add_filter('atbdp_form_preset_widgets', array($this, 'atbdp_form_preset_widgets'));
        add_filter('atbdp_single_listing_other_fields_widget', array($this, 'atbdp_single_listing_content_widgets'));
        add_filter('directorist_field_template', array($this, 'directorist_field_template'), 10, 2);
        add_filter('directorist_single_item_template', array($this, 'directorist_single_item_template'), 10, 2);
        add_filter('directorist_single_section_has_contents', array($this, 'directorist_single_section_has_contents'), 10, 2);
    }

    public function atbdp_single_listing_content_widgets($widgets)
    {
        $widgets['offer_display'] =
            [
                'type' => 'widget',
                'label' => __('Offer Display', 'directorist'),
                'icon' => 'la la-tag',
                'allowMultiple' => false,
                'options' => [
                    'label' => [
                        'type'  => 'text',
                        'label'  => __('Label', 'directorist'),
                        'value' => 'Offers & Coupons',
                    ],
                    'icon' => [
                        'type'  => 'icon',
                        'label'  => __('Icon', 'directorist'),
                        'value' => '',
                    ],
                    'content' => [
                        'type'  => 'textarea',
                        'label'  => __('Content', 'directorist'),
                        'value' => '',
                        'description' => __('You can use any text or shortcode', 'directorist'),
                    ],
                ],
            ];
        return $widgets;
    }

    public function directorist_field_template($template, $field_data)
    {
        if ('iframe' === $field_data['widget_name']) {
            //Helper::get_template_part('listing-form/iframe', $field_data);
        }
        return $template;
    }


    public function directorist_single_item_template($template, $field_data)
    {
        if ('offer_display' === $field_data['widget_name']) {
            atoa_get_template_part('offer-display', $field_data);
        }
        return $template;
    }

    public function directorist_single_section_has_contents($has_content, $data)
    {
        if (isset($data['fields']['offer_display'])) {
            $coupons = new WP_Query(array('post_type' => 'atbdp_offers', 'posts_per_page' => -1, 'post_status' => 'publish', 'no_found_rows' => true, 'fields' => 'ids', 'meta_key' => 'offer_listing', 'meta_value' => get_the_ID(), 'meta_compare' => '='));
            $has_content = $coupons->have_posts() ? true : false;
        }
        return $has_content;
    }
}

new Custom_Offer_Fields;
