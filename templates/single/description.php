<?php

/**
 * @author  mahfuz
 * @since   1.0
 * @version 1.0
 */

$post_id = get_the_title();
$short_description = get_post_meta($post_id, 'offer_short_description', true) ? get_post_meta($post_id, 'offer_short_description', true) : '';
echo $post_id;
?>

<div class="directorit-offer-description">
    <?php echo $short_description; ?>
</div>