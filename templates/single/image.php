<?php

/**
 * @author  mahfuz
 * @since   1.0
 * @version 1.0
 */

?>

<div class="directorit-offer-image">
    <?php
    if (has_post_thumbnail($post_id)) {
        echo get_the_post_thumbnail($post_id, 'thumbnail');
    } else {
        echo '<img src="https://couponplugin.io/wp-content/uploads/2017/10/Hostgator-coupon-codes.jpg">';
    }
    ?>
</div>