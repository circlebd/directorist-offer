<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;
$post_meta = get_post_meta(get_the_ID());
$listings = new WP_Query(array('post_type' => 'at_biz_dir', 'post_status' => 'publish', 'posts_per_page' => -1));
?>

<table class="directorist-input-box widefat" id="directorist-field-details" style="width:100%">

    <tbody>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Select Listing', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group">
                    <select class="offer_listing" name="offer_listing" style="width:99%;max-width:25em;">
                        <option value='0'>Select a listing</option>
                        <?php

                        if ($listings->have_posts()) {
                            while ($listings->have_posts()) : $listings->the_post();
                                $title = get_the_title();
                                $post_id = get_the_ID();
                                // if the post title is too long, truncate it and add "..." at the end
                                $title = (mb_strlen($title) > 50) ? mb_substr($title, 0, 49) . '...' : $title;
                        ?>
                                <option value="<?php echo $post_id; ?>" <?php selected($post_id, $post_meta['offer_listing'][0], true); ?>><?php echo $title; ?></option>
                        <?php
                            endwhile;
                        }
                        ?>
                        </>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Select Offer Type', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <?php $selected_offer_type = isset($post_meta['offer_type']) ? esc_attr($post_meta['offer_type'][0]) : ''; ?>
                <ul class="directorist-radio-list">
                    <li>
                        <div class="directorist-radio directorist-radio-theme-admin directorist-radio-circle">
                            <input id="directorist-coupon" type="radio" name="offer_type" value="coupon" <?php echo checked($selected_offer_type, 'coupon', false); ?>>
                            <label class="directorist-radio__label" for="directorist-coupon"><?php _e('Coupon', 'directorist-offer-addon'); ?></label>
                        </div>
                    </li>
                    <li>
                        <div class="directorist-radio directorist-radio-theme-admin directorist-radio-circle">
                            <input id="directorist-deal" type="radio" name="offer_type" value="deal" <?php echo checked($selected_offer_type, 'deal', false); ?>>
                            <label class="directorist-radio__label" for="directorist-deal"><?php _e('Deal', 'directorist-offer-addon'); ?></label>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Coupon Code', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_coupon_code" type="text" id="offer_coupon_code" value="<?php if (isset($post_meta['offer_coupon_code'])) echo esc_attr($post_meta['offer_coupon_code'][0]); ?>" placeholder="<?php _e('Coupon Code', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Discount Type', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <?php $selected_discount_type = isset($post_meta['offer_discount_type']) ? esc_attr($post_meta['offer_discount_type'][0]) : ''; ?>
                <ul class="directorist-radio-list">
                    <li>
                        <div class="directorist-radio directorist-radio-theme-admin directorist-radio-circle">
                            <input id="offer-percentage" type="radio" name="offer_discount_type" value="percentage" <?php echo checked($selected_discount_type, 'percentage', false); ?>>
                            <label class="directorist-radio__label" for="offer-percentage"><?php _e('Percentage Discount', 'directorist-offer-addon'); ?></label>
                        </div>
                    </li>
                    <li>
                        <div class="directorist-radio directorist-radio-theme-admin directorist-radio-circle">
                            <input id="offer-fixed" type="radio" name="offer_discount_type" value="fixed" <?php echo checked($selected_discount_type, 'fixed', false); ?>>
                            <label class="directorist-radio__label" for="offer-fixed"><?php _e('Fixed Discount', 'directorist-offer-addon'); ?></label>
                        </div>
                    </li>
                    <li>
                        <div class="directorist-radio directorist-radio-theme-admin directorist-radio-circle">
                            <input id="offer-free" type="radio" name="offer_discount_type" value="free" <?php echo checked($selected_discount_type, 'free', false); ?>>
                            <label class="directorist-radio__label" for="offer-free"><?php _e('Free Gift', 'directorist-offer-addon'); ?></label>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Buy - Item', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_buy_item" type="text" id="offer_buy_item" value="<?php if (isset($post_meta['offer_buy_item'])) echo esc_attr($post_meta['offer_buy_item'][0]); ?>" placeholder="<?php _e('Item Name', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>
        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Buy - Quantity', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_buy_qty" type="number" id="offer_buy_qty" value="<?php if (isset($post_meta['offer_buy_qty'])) echo esc_attr($post_meta['offer_buy_qty'][0]); ?>" placeholder="<?php _e('Item Qualtity', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Get - Item', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_get_item" type="text" id="offer_get_item" value="<?php if (isset($post_meta['offer_get_item'])) echo esc_attr($post_meta['offer_get_item'][0]); ?>" placeholder="<?php _e('Item Name', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>
        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Get - Quantity', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_get_qty" type="number" id="offer_get_qty" value="<?php if (isset($post_meta['offer_get_qty'])) echo esc_attr($post_meta['offer_get_qty'][0]); ?>" placeholder="<?php _e('Item Qualtity', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Discount Amount', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group directorist_price-input">
                    <input name="offer_discount_amount" type="text" id="offer_discount_amount" value="<?php if (isset($post_meta['offer_discount_amount'])) echo esc_attr($post_meta['offer_discount_amount'][0]); ?>" placeholder="<?php _e('Discount Amount', 'directorist-offer-addon'); ?>"></label>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-type">
            <td class="directorist-label">
                <label class="directorist-input-box__title widefat"><?php _e('Discount Condition', 'directorist-offer-addon'); ?></label>
            </td>
            <td class="directorist-field-lable">
                <div class="directorist-form-group">
                    <textarea class="textarea" name="offer_discount_condition" placeholder="Description" rows="6" cols="64"><?php if (isset($post_meta['offer_discount_condition'])) echo esc_textarea($post_meta['offer_discount_condition'][0]); ?></textarea>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-instructions">
            <td class="directorist-label">
                <label class="directorist-input-box__title"><?php _e('Offer Short Description', 'directorist-offer-addon'); ?></label>
            </td>
            <td>
                <div class="directorist-form-group ">
                    <textarea class="textarea" name="offer_short_description" placeholder="Description" rows="6" cols="64"><?php if (isset($post_meta['offer_short_description'])) echo esc_textarea($post_meta['offer_short_description'][0]); ?></textarea>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-instructions">
            <td class="directorist-label">
                <label class="directorist-input-box__title"><?php _e('Offer Start Date', 'directorist-offer-addon'); ?></label>
            </td>
            <td>
                <div class="directorist-form-group ">
                    <input name="offer_start_date" type="date" id="offer_start_date" value="<?php if (isset($post_meta['offer_start_date'])) echo esc_attr($post_meta['offer_start_date'][0]); ?>"></label>
                </div>
            </td>
        </tr>

        <tr class="directorist-field-instructions">
            <td class="directorist-label">
                <label class="directorist-input-box__title"><?php _e('Offer End Date', 'directorist-offer-addon'); ?></label>
            </td>
            <td>
                <div class="directorist-form-group ">
                    <input name="offer_end_date" type="date" id="offer_end_date" value="<?php if (isset($post_meta['offer_end_date'])) echo esc_attr($post_meta['offer_end_date'][0]); ?>"></label>
                </div>
            </td>
        </tr>
    </tbody>
</table>