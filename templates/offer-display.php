<?php

/**
 * @author  mahfuz
 * @since   1.0
 * @version 1.0
 */

$coupons = new WP_Query(array('post_type' => 'atbdp_offers', 'posts_per_page' => -1, 'post_status' => 'publish', 'meta_key' => 'offer_listing', 'meta_value' => get_the_ID()));

?>

<div class="directorist-single-info directorist-single-info-iframe <?php echo $data['form_data']['class']; ?>">

    <?php if ($coupons->have_posts()) : ?>
        <?php while ($coupons->have_posts()) : $coupons->the_post(); ?>
            <div class="directorist-offer-single">
                <div class="directorit-offer-body">
                    <!-- Image -->
                    <?php atoa_get_template_part('single/image'); ?>
                    <div class="directorit-offer-info">
                        <!-- Title -->
                        <?php atoa_get_template_part('single/title'); ?>
                        <!-- Description -->
                        <?php atoa_get_template_part('single/description'); ?>
                    </div>
                    <!-- Action -->
                    <?php atoa_get_template_part('single/action'); ?>
                </div>
                <div class="directorit-offer-footer">
                    <!-- Share -->
                    <?php atoa_get_template_part('single/share'); ?>
                    <!-- Like -->
                    <?php atoa_get_template_part('single/like'); ?>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>

</div>