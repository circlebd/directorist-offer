<?php

/**
 * Plugin Name: Directorist -  Offer, Coupon & Deals
 * Plugin URI: https://directorist.com/
 * Description: This is an extension for Directorist plugin. It allows you to create promotional offer, deals and coupons for the listings.
 * Version: 1.0.0
 * Author: wpWax
 * Author URI: https://wpwax.com
 * License: GPLv2 or later
 * Text Domain: directorist-offer-addon
 * Domain Path: /languages
 */
// prevent direct access to the file
defined('ABSPATH') || die('No direct script access allowed!');

if (!class_exists('ATBDP_Offer_Addon')) {
    final class ATBDP_Offer_Addon
    {
        /**
         * @var ATBDP_Offer_Addon
         * @since 1.0
         */

        private static $instance;

        /**
         * Main ATBDP_Offer_Addon Instance
         * 
         * 
         * @return object|ATBDP_Offer_Addon the one true ATBDP_Offer_Addon
         * @uses ATBDP_Offer_Addon::setup_constants() Setup the required constants
         * @uses ATBDP_Offer_Addon::includes() Load reuired files
         * @uses ATBDP_Offer_Addon::load_taxonomies() load the language files
         * @see ATBDP_Offer_Addon
         * @since 1.0.0
         * @static
         * @static_var array $instance
         */

        public static function instance()
        {
            if (!isset(self::$instance) && !(self::$instance instanceof ATBDP_Offer_Addon)) {
                self::$instance = new ATBDP_Offer_Addon;
                self::$instance->setup_constants();
                self::$instance->includes();

                // Enqueue all required scripts
                new ATOA_Enqueuer();
                // add and modify post types
                new ATOA_CPTs();
                // setup data controller
                new ATOA_Data_Controller();
            }
            return self::$instance;
        }

        private function __construct()
        {
            /*making it private prevents constructing the object*/
        }

        /**
         * Setup plugin constance
         * 
         * @access private
         * @return void
         * @since 1.0.0
         */
        private function setup_constants()
        {
            require_once plugin_dir_path(__FILE__) . '/config.php';
        }

        /**
         * It  loads a template file from the Default template directory.
         * @param string $template Name of the file that should be loaded from the template directory.
         * @param array $args Additional arguments that should be passed to the template file for rendering dynamic  data.
         */
        public function load_template($template, $args = array())
        {
            atoa_get_template($template,  $args);
        }

        /** 
         * Includes all requred files for the plugin operation
         * 
         * @access private
         * @return void
         * @since 1.0.0
         */
        private function includes()
        {
            require_once ATOA_INC_DIR . 'helper-functions.php';
            require_once ATOA_CLASSES_DIR . 'class-enqueuer.php';
            require_once ATOA_CLASSES_DIR . 'class-custom-post-types.php';
            require_once ATOA_CLASSES_DIR . 'class-data-controller.php';
            require_once ATOA_CLASSES_DIR . 'class-offer-fields.php';
        }
    }

    function ATBDP_Offer_Addon()
    {
        return ATBDP_Offer_Addon::instance();
    }

    // Instantiate Directorist Offer Addon only if our directorist plugin is active
    if (in_array('directorist/directorist-base.php', (array)get_option('active_plugins'))) {
        ATBDP_Offer_Addon(); // get the plugin running
    }
}
